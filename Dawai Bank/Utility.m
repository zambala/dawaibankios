//
//  Utility.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 15/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "Utility.h"
#import "AppDelegate.h"

@implementation Utility


- (id)init {
    if (self = [super init]) {
        self.baseUrl=@"http://192.168.15.43:3000/";
        
    }
    return self;
}



+ (id)DB {
    static Utility *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}
-(void)inputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler
{
  
    NSString * urlString;
    if(inputArray.count>0)
    {
    NSString * inputRequestString=[NSString stringWithFormat:@"%@",[inputArray objectAtIndex:0]];
    
    NSDictionary *headers = @{ @"content-type": @"application/json",
                               @"cache-control": @"no-cache",
                               @"Accept":@"application/json"
                               
                               };
    
    NSDictionary * params=[[NSDictionary alloc]init];
    @try {
        
        
        if([inputRequestString isEqualToString:@"creatRequest"])
        {
            
            params=@{
                     @"username" :[inputArray objectAtIndex:2],
                     @"email" :[inputArray objectAtIndex:3],
                     @"mobileno" :[inputArray objectAtIndex:4],
                     @"postalcode":@"411028",
                     @"userrole":@"donar"
                     };
            
            urlString=[inputArray objectAtIndex:1];
        }
        else if([inputRequestString isEqualToString:@"updateRequest"])
        {
            
            params=@{
                     @"userrole":[inputArray objectAtIndex:2]
                     };
            
            urlString=[inputArray objectAtIndex:1];
        }
       
        
        
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    @finally {
        NSLog(@"finally");
    }
    
    
    NSData * postData=[NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    self.responseDict=[[NSDictionary alloc]init];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    
                                                    if(data!=nil)
                                                    {
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                            NSLog(@"%@", httpResponse);
                                                            
                                                            if([httpResponse statusCode]==200)
                                                            {
                                                            
                                                              
                                                                
                                                                NSMutableArray * array = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                                
                                                                NSLog(@"Success Output:%@",array);
                                                                NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
                                                                
                                                                [dict setValue:array forKey:@"data"];
                                                                
                                                                
                                                                self.responseDict=[[NSDictionary alloc]init];
                                                                
                                                                self.responseDict=[dict mutableCopy];
                                                                
                                                                handler(self.responseDict);
                                                            
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            
                                                            
                                                            
                                                            
                                                            
                                                        });
                                                        
                                                    }
                                                    }
                                                    else
                                                    {
                                                        handler([NSDictionary new]);
                                                    }
                                                    
                                                    
                                                }];
    [dataTask resume];
    
    
    
    
    
    }
    
    
    
}



@end
