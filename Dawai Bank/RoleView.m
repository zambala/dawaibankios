//
//  RoleView.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 02/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "RoleView.h"
#import <QuartzCore/QuartzCore.h>
#import "VolunteerViewController.h"
#import "DonateMedicineView.h"
#import "Utility.h"

@interface RoleView ()
{
     Utility * sharedManager;
}

@end

@implementation RoleView
{
    NSString * buttonCheckString;
}


- (void)viewDidLoad {
    [super viewDidLoad];
      [self.navigationController setNavigationBarHidden:NO animated:YES];
  
   sharedManager=[Utility DB];
    [self.donarImgView setContentMode:UIViewContentModeScaleAspectFit];
    UIImage * image = [UIImage imageNamed:@"icon_donor.png"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.donarImgView.image = image;
    self.donarImgView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.donarImgView setTintColor:[UIColor lightGrayColor]];
    
    [self.voulanteerImgView setContentMode:UIViewContentModeScaleAspectFit];
    UIImage * image1 = [UIImage imageNamed:@"icon_volunteer.png.png"];
    image1 = [image1 imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.voulanteerImgView.image = image1;
    self.voulanteerImgView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.voulanteerImgView setTintColor:[UIColor lightGrayColor]];
    
    self.letsGoBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.letsGoBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.letsGoBtn.layer.shadowOpacity = 5.0f;
    self.letsGoBtn.layer.shadowRadius = 4.0f;
    self.letsGoBtn.layer.cornerRadius=4.1f;
    self.letsGoBtn.layer.masksToBounds = NO;
    
    self.letsGoBtn.layer.cornerRadius=10.0f;
//    self.letsGoBtn.clipsToBounds = YES;
    
    self.searchBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.searchBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.searchBtn.layer.shadowOpacity = 5.0f;
    self.searchBtn.layer.shadowRadius = 4.0f;
    self.searchBtn.layer.cornerRadius=4.1f;
    self.searchBtn.layer.masksToBounds = NO;
    
    self.searchBtn.layer.cornerRadius=10.0f;
    
    
//    self.searchBtn.clipsToBounds = YES;
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)letsGoAction:(id)sender {
    
    if([buttonCheckString isEqualToString:@""])
    {
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Oops!"
                                                                       message:@"Please select either Donar or Volunteer to continue."
                                                                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * action) {}];
        
        [alert addAction:defaultAction];
        [self presentViewController:alert animated:YES completion:nil];
    }else
    {
        if([buttonCheckString isEqualToString:@"donar"])
        {
          
                
                NSMutableArray * inputArray=[[NSMutableArray alloc]init];
                [inputArray addObject:@"updateRequest"];
                [inputArray addObject:[NSString stringWithFormat:@"%@api/user",sharedManager.baseUrl]];
                [inputArray addObject:[NSString stringWithFormat:@"%@",@"donar"]];
                [sharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                 
                   if(dict)
                   {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        DonateMedicineView * donate = [self.storyboard instantiateViewControllerWithIdentifier:@"DonateMedicineView"];
                        [self.navigationController pushViewController:donate animated:YES];
                    });
                    
                   }
                }];
           
            
          
            
        }else if([buttonCheckString isEqualToString:@"vol"])
        {
            NSMutableArray * inputArray=[[NSMutableArray alloc]init];
            [inputArray addObject:@"updateRequest"];
            [inputArray addObject:[NSString stringWithFormat:@"%@api/user",sharedManager.baseUrl]];
            [inputArray addObject:[NSString stringWithFormat:@"%@",@"voulanteer"]];
            [sharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
                
               if(dict)
               {
                dispatch_async(dispatch_get_main_queue(), ^{
                    VolunteerViewController * vol = [self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerViewController"];
                    [self.navigationController pushViewController:vol animated:YES];
                });
               }
                
                
            }];
            
          
        }
    }
}
- (IBAction)searchAction:(id)sender {
}

- (IBAction)donarAction:(id)sender {
     [self.donarImgView setTintColor:[UIColor colorWithRed:224.0/255 green:107.0/255 blue:146.0/255 alpha:1]];
     [self.voulanteerImgView setTintColor:[UIColor lightGrayColor]];
    self.donarLbl.textColor=[UIColor colorWithRed:224.0/255 green:107.0/255 blue:146.0/255 alpha:1];
    self.voulanteerLbl.textColor=[UIColor lightGrayColor];
    self.descriptionLbl.text=@"Poverty screeches and screams,writes Raymond Downing,but something behind it is singing After spending most of his life confronting povertys scream and trying to discer ernvjrkn vjrkbnvjrtbvjrtbvjurbvjrbvnjrbnvjrbvjkrbvjrtv";
    buttonCheckString=@"donar";
}

- (IBAction)voulanteerAction:(id)sender {
    [self.donarImgView setTintColor:[UIColor lightGrayColor]];
    [self.voulanteerImgView setTintColor:[UIColor colorWithRed:224.0/255 green:107.0/255 blue:146.0/255 alpha:1]];
     self.voulanteerLbl.textColor=[UIColor colorWithRed:224.0/255 green:107.0/255 blue:146.0/255 alpha:1];
     self.donarLbl.textColor=[UIColor lightGrayColor];
    buttonCheckString=@"vol";
    
}

@end
