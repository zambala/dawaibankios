//
//  VolunteerViewController.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 11/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "ViewController.h"

@interface VolunteerViewController : ViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIView *segmentedControlView;
@property (weak, nonatomic) IBOutlet UITableView *volunteerTableView;
@property (weak, nonatomic) IBOutlet UIView *ticketPopUpView;
@property (weak, nonatomic) IBOutlet UIButton *knowMoreButton;

@end
