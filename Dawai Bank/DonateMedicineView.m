//
//  DonateMedicineView.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 04/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "DonateMedicineView.h"
#import "DonateTypeViewController.h"

@interface DonateMedicineView ()

@end

@implementation DonateMedicineView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.proceedBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.proceedBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.proceedBtn.layer.shadowOpacity = 5.0f;
    self.proceedBtn.layer.shadowRadius = 4.0f;
    self.proceedBtn.layer.cornerRadius=4.1f;
    self.proceedBtn.layer.masksToBounds = NO;
    
    self.proceedBtn.layer.cornerRadius=10.0f;
    
    self.companyTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.companyTxtFld.layer.borderWidth= 2.0f;
    self.companyTxtFld.layer.borderColor= [[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.companyTxtFld.layer.cornerRadius=10.0f;
    self.companyTxtFld.layer.masksToBounds=YES;
   
    
    self.medicineTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.medicineTxtFld.layer.borderWidth= 2.0f;
    self.medicineTxtFld.layer.borderColor=[[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.medicineTxtFld.layer.cornerRadius=10.0f;
    self.medicineTxtFld.layer.masksToBounds=YES;
  
    
    self.medicineTypeTxt.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.medicineTypeTxt.layer.borderWidth= 2.0f;
    self.medicineTypeTxt.layer.borderColor=[[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.medicineTypeTxt.layer.cornerRadius=10.0f;
    self.medicineTypeTxt.layer.masksToBounds=YES;
  
    
    self.quantityTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.quantityTxtFld.layer.borderWidth= 2.0f;
    self.quantityTxtFld.layer.borderColor=[[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.quantityTxtFld.layer.cornerRadius=10.0f;
    self.quantityTxtFld.layer.masksToBounds=YES;
   
    
    self.expiryDateTxt.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.expiryDateTxt.layer.borderWidth= 2.0f;
    self.expiryDateTxt.layer.borderColor=[[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.expiryDateTxt.layer.cornerRadius=10.0f;
    self.expiryDateTxt.layer.masksToBounds=YES;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onProceedBtnTap:(id)sender {
    DonateTypeViewController * donate = [self.storyboard instantiateViewControllerWithIdentifier:@"DonateTypeViewController"];
    [self.navigationController pushViewController:donate animated:YES];
}
@end
