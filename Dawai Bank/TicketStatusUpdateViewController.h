//
//  TicketStatusUpdateViewController.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 11/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketStatusUpdateViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIButton *ticketStatusButton;
@property NSString * ticketStatusString;

@end
