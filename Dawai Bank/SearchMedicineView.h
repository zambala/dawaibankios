//
//  SearchMedicineView.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 04/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchMedicineView : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *qtyTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *locationFld;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
- (IBAction)searchAction:(id)sender;
- (IBAction)generateAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *availableLbl;
@property (weak, nonatomic) IBOutlet UIView *blurView;
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UIButton *okBtn;

- (IBAction)okAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *requestIdLbl;
@property (weak, nonatomic) IBOutlet UIButton *genrateBtn;
@property (weak, nonatomic) IBOutlet UITextField *medicineTxtFld;
@end
