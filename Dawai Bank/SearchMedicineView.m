//
//  SearchMedicineView.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 04/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "SearchMedicineView.h"

@interface SearchMedicineView ()

@end

@implementation SearchMedicineView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.availableLbl.hidden=YES;
    self.blurView.hidden=YES;
    self.genrateBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.genrateBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.genrateBtn.layer.shadowOpacity = 5.0f;
    self.genrateBtn.layer.shadowRadius = 4.0f;
    self.genrateBtn.layer.cornerRadius=4.1f;
    self.genrateBtn.layer.masksToBounds = NO;
    
    self.genrateBtn.layer.cornerRadius=10.0f;
    
    self.medicineTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.medicineTxtFld.layer.borderWidth= 2.0f;
    self.medicineTxtFld.layer.borderColor= [[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.medicineTxtFld.layer.cornerRadius=10.0f;
    self.medicineTxtFld.layer.masksToBounds=YES;
    
    self.qtyTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.qtyTxtFld.layer.borderWidth= 2.0f;
    self.qtyTxtFld.layer.borderColor= [[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.qtyTxtFld.layer.cornerRadius=10.0f;
    self.qtyTxtFld.layer.masksToBounds=YES;
    
    self.locationFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.locationFld.layer.borderWidth= 2.0f;
    self.locationFld.layer.borderColor= [[UIColor colorWithRed:243.0/255 green:243.0/255 blue:243.0/255 alpha:1] CGColor];
    self.locationFld.layer.cornerRadius=10.0f;
    self.locationFld.layer.masksToBounds=YES;
    
    self.searchBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.searchBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.searchBtn.layer.shadowOpacity = 5.0f;
    self.searchBtn.layer.shadowRadius = 4.0f;
    self.searchBtn.layer.cornerRadius=4.1f;
    self.searchBtn.layer.masksToBounds = NO;
    
    self.searchBtn.layer.cornerRadius=10.0f;
    
    self.okBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.okBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.okBtn.layer.shadowOpacity = 5.0f;
    self.okBtn.layer.shadowRadius = 4.0f;
    self.okBtn.layer.cornerRadius=4.1f;
    self.okBtn.layer.masksToBounds = NO;
    
    self.okBtn.layer.cornerRadius=10.0f;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.availableLbl.hidden=YES;
    self.blurView.hidden=YES;
     [[self navigationController] setNavigationBarHidden:NO animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)searchAction:(id)sender {
    self.availableLbl.hidden=NO;
}

- (IBAction)generateAction:(id)sender {
    [UIView transitionWithView:self.blurView duration:1.0 options:UIViewAnimationOptionTransitionCurlDown animations:^(void){
        
        [self.blurView setHidden:NO];
        
    } completion:nil];

}
- (IBAction)okAction:(id)sender {
    
      [self.blurView setHidden:YES];
}
@end
