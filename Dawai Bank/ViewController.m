//
//  ViewController.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 01/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Utility.h"
#import "AppDelegate.h"

@interface ViewController ()<UITextFieldDelegate>
{
    Utility * sharedManager;
    AppDelegate * delegate;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sharedManager=[Utility DB];
    // Do any additional setup after loading the view, typically from a nib.
    self.activityInd.hidden=YES;
    self.numberTxtFld.delegate=self;
     self.nameTxtFld.delegate=self;
     self.emailTxtFld.delegate=self;
    [self.navigationController setNavigationBarHidden:YES animated:YES];
//    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self.logoImgView setContentMode:UIViewContentModeScaleAspectFit];
    UIImage * image = [UIImage imageNamed:@"logo_old.png"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.logoImgView.image = image;
    self.logoImgView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.logoImgView setTintColor:[UIColor colorWithRed:(224/255.0) green:(107/255.0) blue:(146/255.0) alpha:1]];
    self.blurView.hidden=YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [self.view addGestureRecognizer:tap];
    self.proceedBtn.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.proceedBtn.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.proceedBtn.layer.shadowOpacity = 5.0f;
    self.proceedBtn.layer.shadowRadius = 4.0f;
    self.proceedBtn.layer.cornerRadius=4.1f;
    self.proceedBtn.layer.masksToBounds = NO;
    self.proceedBtn.layer.cornerRadius=10.0f;
//    self.proceedBtn.clipsToBounds = YES;
    [self.proceedBtn addTarget:self action:@selector(proceedAction) forControlEvents:UIControlEventTouchUpInside];
    self.numberTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.numberTxtFld.layer.borderWidth= 1.0f;
    self.emailTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.emailTxtFld.layer.borderWidth= 1.0f;
    self.nameTxtFld.layer.borderColor=[[UIColor lightTextColor]CGColor];
    self.nameTxtFld.layer.borderWidth= 1.0f;
    self.nameTxtFld.layer.cornerRadius=10.0f;
    self.nameTxtFld.layer.masksToBounds=YES;
    self.numberTxtFld.layer.cornerRadius=10.0f;
    self.numberTxtFld.layer.masksToBounds=YES;
    self.emailTxtFld.layer.cornerRadius=10.0f;
    self.emailTxtFld.layer.masksToBounds=YES;
    UIColor *color = [UIColor lightTextColor];
    self.numberTxtFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    self.nameTxtFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: color}];
    self.numberTxtFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName: color}];
    self.emailTxtFld.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    
   
    
    self.cancelOutlet.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.cancelOutlet.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.cancelOutlet.layer.shadowOpacity = 5.0f;
    self.cancelOutlet.layer.shadowRadius = 4.0f;
    self.cancelOutlet.layer.cornerRadius=4.1f;
    self.cancelOutlet.layer.masksToBounds = NO;
    
    
    self.cancelOutlet.layer.cornerRadius=10.0f;
//    self.cancelOutlet.clipsToBounds = YES;
    
    self.submitOutlet.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.submitOutlet.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.submitOutlet.layer.shadowOpacity = 5.0f;
    self.submitOutlet.layer.shadowRadius = 4.0f;
    self.submitOutlet.layer.cornerRadius=4.1f;
    self.submitOutlet.layer.masksToBounds = NO;
    
    self.submitOutlet.layer.cornerRadius=10.0f;
//    self.submitOutlet.clipsToBounds = YES;
    
//    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
//    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(self.subView.bounds, shadowInsets)];
//     self.subView.layer.shadowPath    = shadowPath.CGPath;
    
    self.subView.layer.cornerRadius = 2;
    self.subView.layer.shadowColor = [[UIColor blackColor] CGColor];
     self.subView.layer.shadowOffset = CGSizeMake(0.5, 4.0); //Here your control your spread
    self.subView.layer.shadowOpacity = 0.5;
    self.subView.layer.shadowRadius = 5.0; //Here your control your blur
    
//    self.subView.backgroundColor=[UIColor colorWithRed:255 green:255 blue:255 alpha:0.5];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)submitBtnAction:(id)sender {
    
    self.activityInd.hidden=NO;
    [self.activityInd startAnimating];
    if(self.nameTxtFld.text.length>0&&self.numberTxtFld.text.length>0&&self.emailTxtFld.text.length>0)
    {
        
        NSMutableArray * inputArray=[[NSMutableArray alloc]init];
        [inputArray addObject:@"creatRequest"];
        [inputArray addObject:[NSString stringWithFormat:@"%@api/user",sharedManager.baseUrl]];
        [inputArray addObject:[NSString stringWithFormat:@"%@",self.nameTxtFld.text]];
        [inputArray addObject:[NSString stringWithFormat:@"%@",self.numberTxtFld.text]];
        [inputArray addObject:[NSString stringWithFormat:@"%@",self.emailTxtFld.text]];
        [inputArray addObject:[NSString stringWithFormat:@"%@",delegate.postalCode]];
        [sharedManager inputRequestMethod:inputArray withCompletionHandler:^(NSDictionary *dict) {
           [self.blurView setHidden:YES];
            NSLog(@"%@",dict);
            
        }];
    }
    
    
}

- (IBAction)cancelAction:(id)sender {
   
        
        [self.blurView setHidden:YES];
        
  
}
-(void)proceedAction
{
    [UIView transitionWithView:self.blurView duration:1.0 options:UIViewAnimationOptionTransitionCurlDown animations:^(void){
        
        [self.blurView setHidden:NO];
        
    } completion:nil];

}
-(void)dismissKeyboard
{
    [self.numberTxtFld resignFirstResponder];
    [self.nameTxtFld resignFirstResponder];
    [self.emailTxtFld resignFirstResponder];
//    [self.countryCodeTF resignFirstResponder];
    //    [self.backView setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    //
    
}
@end
