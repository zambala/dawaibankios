//
//  DonateTypeViewController.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 04/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "DonateTypeViewController.h"
#import "RoleView.h"

@interface DonateTypeViewController ()

@end

@implementation DonateTypeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.backView.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.backView.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.backView.layer.shadowOpacity = 2.0f;
    self.backView.layer.shadowRadius = 2.0f;
    self.backView.layer.cornerRadius=2.1f;
    self.backView.layer.masksToBounds = NO;
    self.backView.layer.cornerRadius=2.0f;
    
    
    self.submitButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.submitButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.submitButton.layer.shadowOpacity = 5.0f;
    self.submitButton.layer.shadowRadius = 4.0f;
    self.submitButton.layer.cornerRadius=10.1f;
    self.submitButton.layer.masksToBounds = NO;
    
    
    self.okButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.okButton.layer.shadowOffset = CGSizeMake(0, 5.0f);
    self.okButton.layer.shadowOpacity = 5.0f;
    self.okButton.layer.shadowRadius = 4.0f;
    self.okButton.layer.cornerRadius=10.1f;
    self.okButton.layer.masksToBounds = NO;
    
    self.popUpView.layer.cornerRadius=10.1f;
    self.popUpView.layer.masksToBounds = NO;
    
    self.courierView.hidden=YES;
    self.pickUpView.hidden=YES;
    self.blurView.hidden=YES;
    
    [self.courierButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [self.courierButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.pickupButton setImage:[UIImage imageNamed:@"radioSelected"] forState:UIControlStateSelected];
    [self.pickupButton setImage:[UIImage imageNamed:@"radioUnselect"] forState:UIControlStateNormal];
    [self.courierButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.pickupButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.submitButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.okButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    self.courierButton.selected=YES;
    self.courierView.hidden=NO;
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}
-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.courierButton)
    {
        self.courierView.hidden=NO;
        self.pickUpView.hidden=YES;
        self.courierButton.selected=YES;
        self.pickupButton.selected=NO;
    }else if (sender == self.pickupButton)
    {
        self.courierView.hidden=YES;
        self.pickUpView.hidden=NO;
        self.courierButton.selected=NO;
        self.pickupButton.selected=YES;
    }else if (sender == self.submitButton)
    {
        self.blurView.hidden=NO;
        if(self.courierButton.selected==YES)
        {
            self.ticketLabel.text=@"Please mention above Ticket ID on your courier.";
        }else if (self.pickupButton.selected==YES)
        {
            self.ticketLabel.text=@"Thank You!! Our volunteer will contact you soon.";
        }
    }else if (sender == self.okButton)
    {
        self.blurView.hidden=YES;
        RoleView * role = [self.storyboard instantiateViewControllerWithIdentifier:@"RoleView"];
        [self.navigationController pushViewController:role animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
