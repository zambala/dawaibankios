//
//  VolunteerViewController.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 11/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "VolunteerViewController.h"
#import "HMSegmentedControl.h"
#import "VolunteerTableViewCell.h"
#import "TicketStatusUpdateViewController.h"

@interface VolunteerViewController ()

@end

@implementation VolunteerViewController
{
    HMSegmentedControl * segmentedControl;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title=@"Volunteer";
    segmentedControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[@"ASSIGNED",@"NOT ASSIGNED"]];
    segmentedControl.frame = CGRectMake(0, 0, self.view.frame.size.width, 56);
    segmentedControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    //    segmentedControl.verticalDividerEnabled = YES;
    //    segmentedControl.verticalDividerColor = [UIColor colorWithRed:(251/255.0) green:(196/255.0) blue:(12/255.0) alpha:1];
    segmentedControl.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleWidth;
    [segmentedControl addTarget:self action:@selector(onSegmentChange) forControlEvents:UIControlEventValueChanged];
    segmentedControl.backgroundColor=[UIColor colorWithRed:(206/255.0) green:(97/255.0) blue:(130/255.0) alpha:1];
    [self.segmentedControlView addSubview:segmentedControl];
    
    self.knowMoreButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.knowMoreButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.knowMoreButton.layer.shadowOpacity = 1.0f;
    self.knowMoreButton.layer.shadowRadius = 1.0f;
    self.knowMoreButton.layer.cornerRadius=15.0f;
    self.knowMoreButton.layer.masksToBounds = NO;
    
    self.ticketPopUpView.hidden=YES;
    [self.knowMoreButton addTarget:self action:@selector(onKnowMoreTap) forControlEvents:UIControlEventTouchUpInside];
    
    self.volunteerTableView.delegate=self;
    self.volunteerTableView.dataSource=self;
    [self.volunteerTableView reloadData];
    
    // Do any additional setup after loading the view.
}
-(void)onSegmentChange
{
    self.volunteerTableView.delegate=self;
    self.volunteerTableView.dataSource=self;
    [self.volunteerTableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    VolunteerTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"VolunteerTableViewCell"];
    cell.statusButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    cell.statusButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    cell.statusButton.layer.shadowOpacity = 1.0f;
    cell.statusButton.layer.shadowRadius = 1.0f;
    cell.statusButton.layer.cornerRadius=15.0f;
    cell.statusButton.layer.masksToBounds = NO;
    if(segmentedControl.selectedSegmentIndex==0)
    {
        NSString * buttonTitle = cell.statusButton.titleLabel.text;
        buttonTitle=@"DELIVERED";
        [cell.statusButton setTitle:[NSString stringWithFormat:@"%@",buttonTitle] forState:UIControlStateNormal];
        if([buttonTitle isEqualToString:@"DELIVERED"])
        {
            [cell.statusButton setBackgroundColor:[UIColor colorWithRed:(172/255.0) green:(212/255.0) blue:(139/255.0) alpha:1]];
        }else if([buttonTitle isEqualToString:@"NOT PICKED UP"])
        {
            [cell.statusButton setBackgroundColor:[UIColor colorWithRed:(198/255.0) green:(198/255.0) blue:(198/255.0) alpha:1]];
        }else if ([buttonTitle isEqualToString:@"PICKED UP"])
        {
            [cell.statusButton setBackgroundColor:[UIColor colorWithRed:(228/255.0) green:(107/255.0) blue:(144/255.0) alpha:1]];
        }
    }else if(segmentedControl.selectedSegmentIndex==1)
    {
        [cell.statusButton setTitle:[NSString stringWithFormat:@"ASSIGN"] forState:UIControlStateNormal];
        [cell.statusButton setBackgroundColor:[UIColor colorWithRed:(228/255.0) green:(107/255.0) blue:(144/255.0) alpha:1]];
    }
    [cell.statusButton addTarget:self action:@selector(onStatusButtonTap) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

-(void)onStatusButtonTap
{
   if(segmentedControl.selectedSegmentIndex==1)
    {
        self.ticketPopUpView.hidden=NO;
    }else if(segmentedControl.selectedSegmentIndex==0)
    {
        TicketStatusUpdateViewController * ticket = [self.storyboard instantiateViewControllerWithIdentifier:@"TicketStatusUpdateViewController"];
        [self.navigationController pushViewController:ticket animated:YES];
    }
}
-(void)onKnowMoreTap
{
    self.ticketPopUpView.hidden=YES;
    TicketStatusUpdateViewController * ticket = [self.storyboard instantiateViewControllerWithIdentifier:@"TicketStatusUpdateViewController"];
    [self.navigationController pushViewController:ticket animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



@end
