//
//  TicketStatusUpdateViewController.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 11/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import "TicketStatusUpdateViewController.h"
#import "VolunteerViewController.h"

@interface TicketStatusUpdateViewController ()

@end

@implementation TicketStatusUpdateViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.updateButton.layer.shadowColor = [[UIColor colorWithRed:0/255 green:0/255 blue:0/255 alpha:0.4] CGColor];
    self.updateButton.layer.shadowOffset = CGSizeMake(0, 2.0f);
    self.updateButton.layer.shadowOpacity = 1.0f;
    self.updateButton.layer.shadowRadius = 1.0f;
    self.updateButton.layer.cornerRadius=15.0f;
    self.updateButton.layer.masksToBounds = NO;
    
    [self.ticketStatusButton setTitle:self.ticketStatusString forState:UIControlStateNormal];
    
    [self.updateButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.ticketStatusButton addTarget:self action:@selector(onButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    // Do any additional setup after loading the view.
}

-(void)onButtonTap:(UIButton*)sender
{
    if(sender == self.updateButton)
    {
        VolunteerViewController * vol = [self.storyboard instantiateViewControllerWithIdentifier:@"VolunteerViewController"];
        [self.navigationController pushViewController:vol animated:YES];
    }else if(sender == self.ticketStatusButton)
    {
        UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Update To:" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [self presentViewController:alert animated:YES completion:^{
            
        }];
        
        UIAlertAction * delivered=[UIAlertAction actionWithTitle:@"DELIVERED" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.ticketStatusButton setTitle:@"DELIVERED" forState:UIControlStateNormal];
            
        }];
        
        UIAlertAction * notpickedup=[UIAlertAction actionWithTitle:@"NOT PICKED UP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.ticketStatusButton setTitle:@"NOT PICKED UP" forState:UIControlStateNormal];
            
        }];
        
        UIAlertAction * pickedUp=[UIAlertAction actionWithTitle:@"PICKED UP" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self.ticketStatusButton setTitle:@"PICKED UP" forState:UIControlStateNormal];
            
        }];
        
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        [alert addAction:delivered];
        [alert addAction:notpickedup];
        [alert addAction:pickedUp];
        [alert addAction:cancel];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
