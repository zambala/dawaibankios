//
//  Utility.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 15/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject
+ (id)DB;
-(void)inputRequestMethod:(NSMutableArray *)inputArray withCompletionHandler:(void (^)(NSDictionary *))handler;
@property NSString * baseUrl;
@property NSDictionary * responseDict;
@end
