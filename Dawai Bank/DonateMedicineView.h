//
//  DonateMedicineView.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 04/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DonateMedicineView : UIViewController
@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UITextField *companyTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *medicineTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *medicineTypeTxt;

@property (weak, nonatomic) IBOutlet UITextField *quantityTxtFld;

@property (weak, nonatomic) IBOutlet UITextField *expiryDateTxt;
- (IBAction)onProceedBtnTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *proceedBtn;

@end
