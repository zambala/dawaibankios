//
//  RoleView.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 02/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoleView : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *donarImgView;
@property (weak, nonatomic) IBOutlet UIImageView *voulanteerImgView;
@property (weak, nonatomic) IBOutlet UILabel *donarLbl;
@property (weak, nonatomic) IBOutlet UILabel *voulanteerLbl;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLbl;
@property (weak, nonatomic) IBOutlet UIButton *letsGoBtn;
- (IBAction)letsGoAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
- (IBAction)searchAction:(id)sender;
- (IBAction)donarAction:(id)sender;
- (IBAction)voulanteerAction:(id)sender;

@end
