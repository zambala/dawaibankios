//
//  main.m
//  Dawai Bank
//
//  Created by Zenwise Technologies on 01/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
