//
//  VolunteerTableViewCell.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 11/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VolunteerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *statusButton;

@end
