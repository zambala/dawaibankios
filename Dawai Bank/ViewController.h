//
//  ViewController.h
//  Dawai Bank
//
//  Created by Zenwise Technologies on 01/05/18.
//  Copyright © 2018 Dawai Bank. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIButton *proceedBtn;
@property (weak, nonatomic) IBOutlet UITextField *nameTxtFld;
@property (weak, nonatomic) IBOutlet UITextField *numberTxtFld;
@property (weak, nonatomic) IBOutlet UIImageView *logoImgView;
@property (weak, nonatomic) IBOutlet UIView *blurView;
- (IBAction)submitBtnAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *cancelOutlet;
@property (weak, nonatomic) IBOutlet UIButton *submitOutlet;
@property (weak, nonatomic) IBOutlet UITextField *emailTxtFld;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityInd;

@property (weak, nonatomic) IBOutlet UIView *subView;
@end

